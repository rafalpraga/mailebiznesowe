<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>maile_biznesowe</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	
	<link href="css/mycss.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

  
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation"> <!-- PANEL NAWIGACYJNY -->
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>
	
	<div class="container-fluid"> <!-- KONTENER STRONY -->
		<div class="row">
			<div class="col-md-8 col-md-offset-2"> <!-- PRAWY DUZY PANEL -->
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Formularz zbierania danych</h3>
					</div>
					<div class="panel-body">
						<form method="POST" action="form_save.php">
							<div class="form-group">
								<label for="mail_biznes">Adres mail'a biznesowego</label>
								<input type="email" class="form-control" id="mail_biznes" name="mail_biznes" readonly value="<?php echo $_GET['mail_biznes']; ?>">
							</div>
							<div class="form-group">
								<label for="mail_biznes">Adres mail'a osoby odpowiedzialnej</label>
								<input type="email" class="form-control" id="mail_user" name="mail_user" placeholder="E-mail">
							</div>
							<div class="form-group">
								<label for="user">Użytkownik odpowiedzialny</label>
								<input type="text" class="form-control" id="user" name="user" placeholder="Imię i nazwisko">
							</div>
							<button type="submit" class="btn btn-default">Submit</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div><!-- KONTENER STRONY - KONIEC -->
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	<!-- holder.js -->
	<script src="js/holder.js"></script>
</body>
</html>