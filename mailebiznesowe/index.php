<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>maile_biznesowe</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	
	<link href="css/mycss.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

  
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation"> <!-- PANEL NAWIGACYJNY -->
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.php">maile_biznesowe</a>
			</div>
		</div><!-- /.container-fluid -->
	</nav>
	
	<div class="container-fluid"> <!-- KONTENER STRONY -->
		<div class="row">
			<div class="col-md-4 col-md-offset-4"> <!-- LEWY PANEL -->
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Wygeneruj<small> i wyślij</small></h3>
					</div>
					<div class="panel-body text-center">
						<?php
							$conn = new mysqli("localhost", "mailebiznesowe", "SD5PdswToFkFdmv6oqma", "mailebiznesowe");
							if ($conn->connect_error) {
								die("Connection failed: " . $conn->connect_error);
							}
							$sql = "SELECT * FROM wysylka ORDER BY id";
							$result = $conn->query($sql);
							
							$fp = fopen('rap/file.csv', 'w');
							
							while($row = mysqli_fetch_assoc($result))
								{
									fputcsv($fp, $row, ";");
								}

							fclose($fp);
							
							echo "Przygotowano plik z bazą maili biznesowych i ich domniemanych właścicieli.<br />W bazie jest <strong>".mysqli_num_rows($result)."</strong> wierszy.";
							
							/*
							if (mysqli_num_rows($result) > 0) 
							{
								echo "<table class=\"table\">";
								echo "<tr><td>Mail biznesowy</td><td>User</td><td>Mail</td></tr>";
								while($row = mysqli_fetch_assoc($result))
								{
									echo "<tr>";
									echo "<td>".$row['mailbiznesowy']."</td><td>".$row['uzytkownik']."</td><td>".$row['mailkontaktowy']."</td>";
									echo "</tr>";
								}
								echo "</table>";
							}
							*/
						?>
						<form action="send_mail.php" method="post">
						<h3>Wpisz hasło i kliknij poniżej aby wysłać bazę na maila</h3>
						<div class="form-group">
							<label for="pass">Hasło:</label>
							<input type="password" class="form-control" name="pass" id="pass" placeholder="password">
						</div>
						<button type="submit" class="btn btn-default">Wyślij</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div><!-- KONTENER STRONY - KONIEC -->
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	<!-- holder.js -->
	<script src="js/holder.js"></script>
</body>
</html>